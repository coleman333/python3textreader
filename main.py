import cv2
import pytesseract
import numpy as np
from scipy import stats
from math import log
from matplotlib import pyplot as plt
from PIL import Image

def resize_img(img, scale):
    w_scale, h_scale = int(img.shape[1] / scale), int(img.shape[0] / scale)
    return cv2.resize(img, (w_scale, h_scale))

original = cv2.imread('image1.jpg', cv2.IMREAD_COLOR)
img = cv2.imread('image1.jpg', cv2.IMREAD_GRAYSCALE)
# img_ravel = img.ravel()

# low = int(img_ravel.mean() - img_ravel.std())
# high = int(img_ravel.mean() + img_ravel.std())

# mask = cv2.inRange(img, low, high)


cv2.imshow('gray', resize_img(img, 3.3))
# cv2.imshow('thresh1', resize_img(mask, 3.3))

th2 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,13,4)
th3 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,13,4)

th2 = cv2.bitwise_not(th2)
th3 = cv2.bitwise_not(th3)

kernel = np.ones((3,3),np.uint8)
opening2 = cv2.morphologyEx(th2, cv2.MORPH_OPEN, kernel)
opening2 = cv2.dilate(opening2, (5, 5),iterations = 5)

# opening3 = cv2.morphologyEx(th3, cv2.MORPH_OPEN, kernel)

# cv2.imshow('thresh2', resize_img(th2, 3.3))
# cv2.imshow('thresh3', resize_img(th3, 3.3))

trhesh2 = cv2.bitwise_not(opening2)

_, contours, _ = cv2.findContours(trhesh2, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

for cnt in contours:
    if cv2.contourArea(cnt) > 3000:
        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        cv2.drawContours(original,[box],0,(0,0,255),2)

# cv2.imshow('opening2', resize_img(opening2, 3.3))
# cv2.imshow('opening3', resize_img(opening3, 3.3))
cv2.imshow('trhesh2', resize_img(trhesh2, 3.3))
cv2.imshow('original', resize_img(original, 3.3))

# cv2.imwrite('trhesh2.jpg', trhesh2)

# img = Image.fromarray(trhesh2)
# txt = pytesseract.image_to_string(img)
# print(txt)





# cap = cv2.VideoCapture(0)
# kernel = np.ones((3,3),np.uint8)

# while(True):
#     # Capture frame-by-frame
#     ret, frame = cap.read()

#     # Our operations on the frame come here
#     gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#     th2 = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,13,4)
#     th2 = cv2.bitwise_not(th2)
#     opening2 = cv2.morphologyEx(th2, cv2.MORPH_OPEN, kernel)
#     trhesh2 = cv2.bitwise_not(opening2)

#     # Display the resulting frame
#     cv2.imshow('frame', trhesh2)
#     if cv2.waitKey(1) & 0xFF == ord('q'):
#         break

cv2.waitKey()
cv2.destroyAllWindows()